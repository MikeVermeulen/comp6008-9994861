﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Maths_Problem
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        void Button_Clicked(object Sender, EventArgs e)
        {
            var i = 0;
            var sum = 0;

            int userNumber = int.Parse(UserEntry.Text);

            for (i = 0; i < userNumber; i++)
            {
                if ((i % 3) == 0 || (i % 5) == 0)
                {
                    sum = sum + i;
                }
            }
            Sum.Text = $"The sum is {sum}";

        }
    }
}
