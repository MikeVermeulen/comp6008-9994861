﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Random_Number
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            
        }

        void Button_Clicked(object Sender, EventArgs e)
        {
           

            Random random = new Random();
            int randomNumber = random.Next(0, 10);

            int userNumber = int.Parse(UserEntry.Text);

            if (userNumber == randomNumber)
            {
                Background.BackgroundColor = Color.Green;
            }
            else
            {
                Background.BackgroundColor = Color.Red;
            }

           
        }
    }
}
